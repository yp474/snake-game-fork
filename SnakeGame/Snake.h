#pragma once
#include <SFML/Graphics.hpp>
#include <iostream>
#include <vector>
#include "SnakePart.h"

enum class GameState { GAME_ON, GAME_OVER, PAUSE, TURN };

class Snake
{
public:
	Snake(float length, sf::Color color, float velocity);
	~Snake();

	void DrawSnake(sf::RenderWindow& window);
	void Move();
	GameState Update(float windowLen);
	bool TryToEat(sf::RectangleShape apple);
	bool IsSquareFree(sf::Vector2f pos);

private:
	void AddNewPart(sf::RectangleShape apple);

private:
	std::vector<SnakePart> _snakeParts;
	float _velocity;
};

