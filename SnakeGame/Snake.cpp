#include "Snake.h"

Snake::Snake(float length, sf::Color color, float velocity)
{
	_velocity = velocity;
	_snakeParts.push_back(SnakePart(length, color, sf::Vector2f(length / 2, length / 2)));
	_snakeParts.push_back(SnakePart(length, color, sf::Vector2f(3 * (length / 2), length / 2)));
	_snakeParts.push_back(SnakePart(length, color, sf::Vector2f(5 * (length / 2), length / 2)));
	_snakeParts.push_back(SnakePart(length, color, sf::Vector2f(7 * (length / 2), length / 2)));
}

Snake::~Snake()
{
}

void Snake::DrawSnake(sf::RenderWindow& window)
{
	for (SnakePart& snakePart : _snakeParts)
		snakePart.Draw(window);
}

void Snake::Move()
{
	for (unsigned int i = 0; i < _snakeParts.size() - 1; i++) {
		_snakeParts[i].Move(_snakeParts[i + 1].GetPosition());
	}

	_snakeParts[_snakeParts.size() - 1].Move(_velocity);
}

GameState Snake::Update(float windowLen)
{
	Direction firstPartDirection = _snakeParts[_snakeParts.size() - 1].GetDirection();
	sf::Vector2f firstPartPos = _snakeParts[_snakeParts.size() - 1].GetPosition();

	//check for death by colliding wall
	if (firstPartPos.x < 0 || firstPartPos.x > windowLen || firstPartPos.y < 0 || firstPartPos.y > windowLen)
		return GameState::GAME_OVER;
	//check for death by colliding itself
	for (unsigned int i = 0; i < _snakeParts.size() - 1; i++) {
		if (_snakeParts[i].GetPosition() == firstPartPos)
			return GameState::GAME_OVER;
	}

	//check for user input
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::Space)) {
		return GameState::PAUSE;
	}
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::W) && firstPartDirection != Direction::DOWN && firstPartDirection != Direction::UP && !sf::Keyboard::isKeyPressed(sf::Keyboard::A) && !sf::Keyboard::isKeyPressed(sf::Keyboard::D)) {
		_snakeParts[_snakeParts.size() - 1].SetDirection(Direction::UP);
		return GameState::TURN;
	}	
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::S) && firstPartDirection != Direction::UP && firstPartDirection != Direction::DOWN && !sf::Keyboard::isKeyPressed(sf::Keyboard::A) && !sf::Keyboard::isKeyPressed(sf::Keyboard::D)) {
		_snakeParts[_snakeParts.size() - 1].SetDirection(Direction::DOWN);
		return GameState::TURN;
	}
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::A) && firstPartDirection != Direction::RIGHT && firstPartDirection != Direction::LEFT && !sf::Keyboard::isKeyPressed(sf::Keyboard::W) && !sf::Keyboard::isKeyPressed(sf::Keyboard::S)) {
		_snakeParts[_snakeParts.size() - 1].SetDirection(Direction::LEFT);
		return GameState::TURN;
	}	
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::D) && firstPartDirection != Direction::LEFT && firstPartDirection != Direction::RIGHT && !sf::Keyboard::isKeyPressed(sf::Keyboard::W) && !sf::Keyboard::isKeyPressed(sf::Keyboard::S)) {
		_snakeParts[_snakeParts.size() - 1].SetDirection(Direction::RIGHT);
		return GameState::TURN;
	}	

	return GameState::GAME_ON;
}

bool Snake::TryToEat(sf::RectangleShape apple)
{
	sf::Vector2f firstPartPos = _snakeParts[_snakeParts.size() - 1].GetPosition();

	if (firstPartPos == apple.getPosition()) {
		this->AddNewPart(apple);
		return true;
	}

	return false;
}

bool Snake::IsSquareFree(sf::Vector2f pos)
{
	for (int i = 0; i < _snakeParts.size(); i++) {
		if (_snakeParts[i].GetPosition() == pos)
			return false;
	}

	return true;
}

void Snake::AddNewPart(sf::RectangleShape apple)
{
	sf::Vector2f lastPartPos = _snakeParts[0].GetPosition();

	_snakeParts.push_back(_snakeParts[_snakeParts.size() - 1]);
	for (int i = _snakeParts.size() - 2; i > 0; i--)
		_snakeParts[i] = _snakeParts[i - 1];

	_snakeParts[0] = SnakePart(apple.getSize().x, sf::Color::Green, sf::Vector2f(lastPartPos));
}
