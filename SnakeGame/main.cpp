#pragma warning(disable : 4996)

#include <iostream>
#include <SFML/Graphics.hpp>
#include <SFML/Audio.hpp>
#include <stdlib.h>
#include <time.h>
#include "Snake.h"
#include "Menu.h"

#define WINDOW_LEN 512
#define SQUARE_LEN 32.f
#define NUM_OF_SQUARES (WINDOW_LEN / (int)SQUARE_LEN)
#define TIME_PER_MOVE 0.13f

void SpawnApple(Snake& snake, sf::RectangleShape& apple);
void Pause(int scoreValue, sf::Text score, Snake& snake, sf::RectangleShape& apple);
void RunMenu(Menu& menu);
bool RunGame();

sf::Font textFont, numberFont;

sf::RenderWindow window(sf::VideoMode(WINDOW_LEN, WINDOW_LEN), "Snake");

int main()
{
    srand((unsigned int)time(NULL));
    
    textFont.loadFromFile("MilkyNice.ttf");
    numberFont.loadFromFile("ArialCEBold.ttf");

    Menu startingMenu(sf::Vector2f(window.getSize()), textFont, "SNAKE", "PLAY", sf::Color::Green);
    Menu gameOverMenu(sf::Vector2f(window.getSize()), textFont, "GAME OVER", "PLAY AGAIN", sf::Color::Red);

    sf::Music music;
    music.openFromFile("sounds/music1.wav");
    music.setLoop(true);
    music.play();
    RunMenu(startingMenu);
    while (window.isOpen()) {
        if (RunGame())
            RunMenu(gameOverMenu);
    }

    return 0;
}

bool RunGame() {
    Snake snake(SQUARE_LEN, sf::Color::Green, SQUARE_LEN);
    sf::Text score;
    sf::RectangleShape apple;
    int scoreValue = 4;
    sf::Clock clock;
    float deltaTime = 0.f;
    GameState gameState = GameState::GAME_ON;
    sf::Sound eatingSound;
    sf::SoundBuffer eatingSoundBuffer;
    
    //initialization
    {  
        eatingSoundBuffer.loadFromFile("sounds/eatApple.wav");
        eatingSound.setBuffer(eatingSoundBuffer);
        //apple
        apple = sf::RectangleShape(sf::Vector2f(SQUARE_LEN, SQUARE_LEN));
        apple.setFillColor(sf::Color::Red);
        apple.setOrigin(apple.getSize() / 2.f);
        SpawnApple(snake, apple);

        //score
        {
            score.setColor(sf::Color(155, 155, 155));
            score.setFont(numberFont);
            score.setString(std::to_string(scoreValue));
            score.setPosition(WINDOW_LEN / 2.f, WINDOW_LEN / 2.f);
            score.setCharacterSize(200);
            score.setOrigin(score.getLocalBounds().left + score.getLocalBounds().width / 2.0f, score.getLocalBounds().top + score.getLocalBounds().height / 2.0f);
        }
    }

    sf::Event event;
    while (window.isOpen())
    {
        deltaTime += clock.restart().asSeconds();

        //handle events
        while (window.pollEvent(event))
        {
            if (event.type == sf::Event::Closed)
                window.close();
        }

        //update snake
        gameState = snake.Update((float)window.getSize().x);
        if (gameState == GameState::GAME_OVER)
            return true;
        else if (gameState == GameState::PAUSE)
            Pause(scoreValue, score, snake, apple);

        //check for apple eating
        if (snake.TryToEat(apple)) {
            //spawn new apple
            eatingSound.play();
            SpawnApple(snake, apple);

            score.setString(std::to_string(++scoreValue));
            if (scoreValue == 10 || scoreValue == 100)
                score.setOrigin(score.getLocalBounds().left + score.getLocalBounds().width / 2.0f, score.getLocalBounds().top + score.getLocalBounds().height / 2.0f);
        }

        //check if it is time to move
        if (deltaTime > TIME_PER_MOVE) {
            snake.Move();
            deltaTime = 0.f;
        }

        window.clear();

        window.draw(score);
        snake.DrawSnake(window);
        window.draw(apple);

        window.display();
    }

    return false;
}

void SpawnApple(Snake& snake, sf::RectangleShape& apple) {
    float appleX, appleY;

    do {
        appleX = rand() % NUM_OF_SQUARES * SQUARE_LEN + SQUARE_LEN / 2;
        appleY = rand() % NUM_OF_SQUARES * SQUARE_LEN + SQUARE_LEN / 2;
    } while (!snake.IsSquareFree(sf::Vector2f(appleX, appleY)));

    apple.setPosition(appleX, appleY);
}

void Pause(int scoreValue, sf::Text score, Snake& snake, sf::RectangleShape& apple) {
    sf::RectangleShape pauseScreen(sf::Vector2f(WINDOW_LEN, WINDOW_LEN));
    pauseScreen.setFillColor(sf::Color(255 ,255, 255, 50));

    sf::CircleShape triangle(80.f, 3);
    triangle.setOrigin(triangle.getRadius(), triangle.getRadius());
    triangle.setPosition(WINDOW_LEN / 2.f, WINDOW_LEN / 2.f);
    triangle.setRotation(90);

    if (scoreValue < 10)
        score.setPosition(35.f, 45.f);
    else if (scoreValue < 100)
        score.setPosition(50.f, 45.f);
    else
        score.setPosition(75.f, 45.f);

    score.setCharacterSize(80);
    score.setOrigin(score.getLocalBounds().left + score.getLocalBounds().width / 2.0f, score.getLocalBounds().top + score.getLocalBounds().height / 2.0f);

    do {
        sf::Event event;
        while (window.pollEvent(event))
        {
            if (event.type == sf::Event::Closed)
                window.close();
        }

        window.clear();
        snake.DrawSnake(window);
        window.draw(apple);
        window.draw(pauseScreen);
        window.draw(triangle);
        window.draw(score);
        window.display();
    } while (!sf::Keyboard::isKeyPressed(sf::Keyboard::W) && !sf::Keyboard::isKeyPressed(sf::Keyboard::A) && !sf::Keyboard::isKeyPressed(sf::Keyboard::S) && !sf::Keyboard::isKeyPressed(sf::Keyboard::D) && window.isOpen());

    score.setPosition(WINDOW_LEN / 2.f, WINDOW_LEN / 2.f);
    score.setCharacterSize(200);
    score.setOrigin(score.getLocalBounds().left + score.getLocalBounds().width / 2.0f, score.getLocalBounds().top + score.getLocalBounds().height / 2.0f);
}

void RunMenu(Menu& menu) {
    bool isMenu = true;
    sf::Event event;
    MenuState menuState;

    while (window.isOpen() && isMenu) {
        //handle events
        while (window.pollEvent(event))
        {
            if (event.type == sf::Event::Closed)
                window.close();
        }
        
        menuState = menu.ManageButtons(window);

        if (menuState == MenuState::EXIT)
            window.close();
        else if (menuState == MenuState::PLAY)
            isMenu = false;

        window.clear();
        menu.Draw(window);
        window.display();
    }
}