#pragma once
#include <SFML/Graphics.hpp>

class Button
{
public:
	Button(std::string text, sf::Vector2f pos, sf::Color textColor, sf::Color bgColor, int charSize, sf::Vector2f size, sf::Font& font);
	Button() { _button = sf::RectangleShape(sf::Vector2f(0.f, 0.f)); _text = sf::Text(); }
	~Button();

	bool ManageButton(sf::RenderWindow& window);
	void Draw(sf::RenderWindow& window);

private:
	sf::RectangleShape _button;
	sf::Text _text;
};