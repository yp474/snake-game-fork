#include "Button.h"

Button::Button(std::string text, sf::Vector2f pos, sf::Color textColor, sf::Color bgColor, int charSize, sf::Vector2f size, sf::Font& font)
{
	_text.setString(text);
	_text.setFont(font);
	_text.setCharacterSize(charSize);
	_text.setFillColor(textColor);
	_text.setPosition(pos);
	_text.setOrigin(_text.getLocalBounds().left + _text.getLocalBounds().width / 2.0f, _text.getLocalBounds().top + _text.getLocalBounds().height / 2.0f);

	_button.setPosition(pos);
	_button.setSize(size);
	_button.setOrigin(size.x / 2.f, size.y / 2.f);
	_button.setFillColor(bgColor);
	_button.setOutlineColor(textColor);
	_button.setOutlineThickness(3.f);
}

Button::~Button()
{
}

bool Button::ManageButton(sf::RenderWindow& window)
{
	float mouseX = sf::Mouse::getPosition(window).x;
	float mouseY = sf::Mouse::getPosition(window).y;

	sf::Vector2f btnTopLeft = sf::Vector2f(_button.getPosition().x - _button.getSize().x / 2.f, _button.getPosition().y - _button.getSize().y / 2.f);
	sf::Vector2f btnBottomRight = sf::Vector2f(_button.getPosition().x + _button.getSize().x / 2.f, _button.getPosition().y + _button.getSize().y / 2.f);

	if (mouseX > btnTopLeft.x && mouseX < btnBottomRight.x &&
		mouseY > btnTopLeft.y && mouseY < btnBottomRight.y) {
		_text.setFillColor(sf::Color::Green);
		_button.setOutlineColor(sf::Color::Green);

		if (sf::Mouse::isButtonPressed(sf::Mouse::Left))
			return true;
	}
	else {
		_text.setFillColor(sf::Color::White);
		_button.setOutlineColor(sf::Color::White);
	}

	return false;
}

void Button::Draw(sf::RenderWindow& window) {
	window.draw(_button);
	window.draw(_text);
}