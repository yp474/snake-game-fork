#include "SnakePart.h"

SnakePart::SnakePart(float length, sf::Color color, sf::Vector2f position, Direction dir)
{
	_body.setSize(sf::Vector2f(length, length));
	_body.setFillColor(color);
	_body.setOrigin(length / 2.f, length / 2.f);
	_body.setPosition(position);
	_direction = dir;
}

SnakePart::~SnakePart()
{
}

void SnakePart::Draw(sf::RenderWindow& window)
{
	window.draw(_body);
}

void SnakePart::Move(float velocity)
{
	switch (_direction) {
	case Direction::UP:
		_body.move(0.f, -velocity);
		break;
	case Direction::DOWN:
		_body.move(0.f, velocity);
		break;
	case Direction::LEFT:
		_body.move(-velocity, 0.f);
		break;
	case Direction::RIGHT:
		_body.move(velocity, 0.f);
		break;
	}
}
